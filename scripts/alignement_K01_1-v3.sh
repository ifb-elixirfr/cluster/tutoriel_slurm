#!/bin/bash

#SBATCH --cpus-per-task=10
#SBATCH --mem=1G
#SBATCH --time=05:00
#SBATCH --output=alignment-%j.out
#SBATCH --error=alignment-%j.err
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=mon@email.fr

module load bowtie2/2.4.4 samtools/1.15.1

reference_index="/shared/bank/arabidopsis_thaliana/TAIR10.1/bowtie2/Arabidopsis_thaliana.TAIR10.1_genomic"
file_id="KO1_1"

mkdir -p results

srun bowtie2 --threads="${SLURM_CPUS_PER_TASK}" -x "${reference_index}" -U "data/${file_id}.fastq.gz" -S "${file_id}.sam"
srun -J "filter" samtools view -hbS -q 30 -o "results/${file_id}.filtered.bam" "results/${file_id}.sam"
srun -J "sort" samtools sort -o "results/${file_id}.bam" "results/${file_id}.filtered.bam"

rm "${file_id}.sam" "${file_id}.filtered.bam"