{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Si vous avez déjà réalisé ce tutoriel, vous pouvez lancer la commande suivante pour nettoyer votre espace de travail, puis passer au chapitre suivant_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rm -rf results *.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction à SLURM"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SLURM (Simple Linux Utility for Resource Management) est le logiciel utilisé pour assurer la gestion des ressources sur le cluster de calcul de l’IFB.\n",
    "Il permet de réserver des ressources et lancer des programmes sur les noeuds de calcul du cluster.\n",
    "Les commandes SLURM commencent toutes par la lettre `s`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RTFM\n",
    "\n",
    "L'IFB propose une doc pas si mal qu'il serait bienvenue de consulter en famille : https://ifb-elixirfr.gitlab.io/cluster/doc/ \n",
    "\n",
    "Et notamment des sections dédiées à SLURM, des tutoriaux et des cookbooks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "## Le vocabulaire SLURM\n",
    "\n",
    "<img src=\"images/usine.png\" alt=\"drawing\" width=\"300\" style=\"float: right;\"/>\n",
    "\n",
    "### CPU : le CPU est la plus petite unité d’un processeur d’ordinateur.\n",
    "Cette unité correspond généralement à un thread ou un coeur hyperthreadé.\n",
    "\n",
    "À l’IFB sur la plupart des noeuds nous avons 2 processeurs Haswell E5-2695 v3.  \n",
    "Chaque processeur a 14 coeurs physiques et 28 threads (ou coeurs virtuels)  \n",
    "SLURM considère donc **56 CPU** par noeud.\n",
    "\n",
    "### RAM : la RAM est la mémoire utilisée par le processeur pour stocker les données analysées\n",
    "\n",
    "À l’IFB nous avons en moyenne 252 Go de mémoire vive par noeud\n",
    "\n",
    "### Task : une task est un processus (exécution d’un outil). Elle peut utiliser plusieurs CPU.\n",
    "\n",
    "### Job : un job est une réservation de ressources (CPU, RAM et tasks) pour effectuer une analyse.\n",
    "\n",
    "On peut imaginer un job comme **une usine** dans laquelle on va organiser l'exécution de son analyse.\n",
    "\n",
    "La taille de l’usine va être définie par une quantité de CPU, de RAM et de tasks (on définit généralement le nombre de CPU en fonction du nombre de tasks).\n",
    "\n",
    "<img src=\"images/chaine.png\" alt=\"drawing\" width=\"300\" style=\"float: right;\"/>\n",
    "\n",
    "### Job step : un “job step” est la partie d’un job qui consiste à exécuter un programme\n",
    "\n",
    "Un “job step” peut utiliser plusieurs “tasks” mais ceci est spécifique à certains programmes compatibles avec le calcul parallèle. Dans la plupart des cas, un job step utilise une “task”.\n",
    "\n",
    "On peut imaginer un job step comme **un atelier de la chaîne de production** dans notre usine.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Maîtriser SLURM\n",
    "\n",
    "Au travers de l’utilisation des commandes SLURM nous allons apprendre à réserver des ressources (une usine) et mettre en route des chaînes de production pour nos analyses.\n",
    "\n",
    "Plus nous aurons de grandes chaînes de production et plus nos ateliers seront gourmands en ressources, plus nous aurons besoin de réserver de grandes usines !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Soumettre un \"job\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lancer des jobs ou analyse interactive\n",
    "\n",
    "Il existe deux modes d'utilisation d'une infrastructure de calcul :\n",
    "\n",
    "| Analyse par soumission de jobs | Analyse interactive |\n",
    "| :- | :- |\n",
    "| Nécessite l’écriture de scripts | L’utilisateur est intégré au coeur du processus d’analyse |\n",
    "| Est adaptée pour des traitements longs et/ou répétitifs | Est adaptée aux traitements courts et uniques |\n",
    "| Est adaptée aux outils non interactifs (ne nécessitant pas d’interaction avec un humain) | Est adaptée aux outils interactifs (nécessitant une interaction avec un humain) |\n",
    "\n",
    "Nous allons apprendre à soumettre des jobs.\n",
    "\n",
    "👆 L'usage d'un script est recommandé pour la reproductibilité car il est garde une trace de l'analyse <img src=\"images/FAIR_data_principles.png\" alt=\"FAIR\" width=\"100\" style=\"float: right;\"/> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Les commandes `sbatch` et `srun`\n",
    "\n",
    "Une analyse bioinformatique que l’on souhaite lancer sur le cluster se présente sous la forme **d’un ou plusieurs scripts shell**.\n",
    "\n",
    "Pour soumettre un job sur le cluster on utilise principalement deux commandes SLURM :\n",
    "* `sbatch` permet de réserver des ressources (l’usine) et demander le lancement de son script\n",
    "* `srun` permet de lancer un “job step” dans son script\n",
    "\n",
    "### Les options les plus utiles\n",
    "\n",
    "`sbatch` et `srun` proposent les mêmes options de base :\n",
    "\n",
    "`--cpus-per-task` : Nombre de CPU par tasks\n",
    "\n",
    "`--mem` : Quantité de mémoire vive allouée par noeud de calcul (exprimée en Mo par défaut)\n",
    "\n",
    "`--mem-per-cpu` : Quantité de mémoire exprimée en fonction du nombre de CPU\n",
    "\n",
    "`--ntasks` : Nombre de tasks (par défaut un job est dimensionné avec 1 task)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Récupérer les données d'exemple\n",
    "\n",
    "Nous allons utiliser 6 séquences pour illuster nos exemples d'utilisation de SLURM.\n",
    "\n",
    "Ces séquences sont disponibles sur Zenodo [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5035749.svg)](https://doi.org/10.5281/zenodo.5035749)\n",
    "\n",
    "Nous allons utiliser l'outil `zenodo_get` pour télécharger ces données :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "module load zenodo_get\n",
    "zenodo_get -o data -t 60 -R 3 10.5281/zenodo.5035749"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/petite-chaine.png\" alt=\"drawing\" width=\"200\" style=\"float: right;\"/>\n",
    "\n",
    "## Effectuer un alignement de séquences\n",
    "\n",
    "\n",
    "Nous allons utiliser deux outils de bioinformatique pour réaliser notre première analyse avec SLURM :\n",
    "* **bowtie2** : permet d’aligner une séquence sur un génome de référence. On utilise un index réalisé à partir du génome de référence pour effectuer l’alignement\n",
    "* **samtools** : permet de filtrer, trier et convertir une cartographie d’alignement de séquences\n",
    "\n",
    "<img src=\"images/illustration-strategies-parallele.png\" alt=\"drawing\" width=\"400\" style=\"float: right;\"/>\n",
    "\n",
    "![organigramme Job-Array](images/organigramme-job-array.png)\n",
    "\n",
    "💡Le job sera exécuté 6 fois (`--array=0-5`) et pour chaque exécution, il traitera un fichier différent en fonction de l'index du job courant `${files[$SLURM_ARRAY_TASK_ID]}`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat scripts/alignement.sh | pygmentize -f terminal -l bash"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous soumettons le script au cluster à l'aide de la commande sbatch en précisnat nos besoins de ressources :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_id=`sbatch --parsable scripts/alignement.sh`\n",
    "echo $job_id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La soumission du job a entraîné la création d’un fichier `slurm-<job_id>.out`. Ce fichier contient le résultat des commandes executées au sein du job.\n",
    "\n",
    "(Optionel: `--parsable` nous sert ici juste à récupérer le `job_id` pour un usage ultérieur)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Bonnes pratiques pour la reproductibilité 😇"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/FAIR_data_principles.png\" alt=\"FAIR\" width=\"100\" style=\"float: right;\"/>\n",
    "\n",
    "### Les paramètres de SLURM\n",
    "\n",
    "Les paramètres pour `sbatch` peuvent être donner :\n",
    "* dans la ligne de commande de `sbatch` au moment de le lancer\n",
    "* en entête du script `sbatch`\n",
    "\n",
    "Afin de faciliter la réutilisabilité et reproductibilité de ce script, il est en effet recommandé de les déclarer directement dans le préambule du script :\n",
    "\n",
    "```bash\n",
    "#SBATCH --cpus-per-task=10\n",
    "#SBATCH --mem=1G\n",
    "```\n",
    "\n",
    "Ainsi, lorsque l'on reprendra ce script après quelques jours, mois ou semaine, il ne sera pas nécessaire de se souvenir de la réservation de ressources nécessaire pour l'exécuter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/FAIR_data_principles.png\" alt=\"FAIR\" width=\"100\" style=\"float: right;\"/>\n",
    "\n",
    "### Préciser les versions des outils\n",
    "\n",
    "`module` chargera la dernière version de l'outil si vous ne lui précisez pas de version\n",
    "\n",
    "```bash\n",
    "module load bowtie2 samtools\n",
    "module list\n",
    "    Currently Loaded Modulefiles:\n",
    "    1) bowtie2/2.4.4   2) samtools/1.15.1\n",
    "```\n",
    "\n",
    "Mais il est important de tracer les versions des outils que vous utilisez pour y revenir plus tard ou pour l'écriture du matériels et méthodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/FAIR_data_principles.png\" alt=\"FAIR\" width=\"100\" style=\"float: right;\"/>\n",
    "\n",
    "### Usage des variables SLURM\n",
    "\n",
    "SLURM et `sbatch` propose également des variables d'environnement que l'on peut utiliser au sein d'un job pour connaitre la réservation effectuer.  \n",
    "On peut ainsi utiliser la variable `SLURM_CPUS_PER_TASK` pour connaitre le nombre de CPU par tâche réservés pour ce job.\n",
    "\n",
    "C'est très utile pour l'exécution de la commande `bowtie2` pour laquelle on doit spécifier le nombre de threads à utiliser :\n",
    "```bash\n",
    "srun bowtie2 --threads=\"${SLURM_CPUS_PER_TASK}\" -x \"${reference_index}\" -U \"${file_id}.fastq.gz\" -S \"${file_id}.sam\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suivi de l’utilisation des ressources avec sacct\n",
    "\n",
    "### `acct`\n",
    "\n",
    "SLURM intègre un mécanisme pour suivre la consommation des ressources de chaque job.\n",
    "\n",
    "Les jobs ne respectant pas leur réservation de ressources peuvent être tués automatiquement par le cluster.\n",
    "\n",
    "La commande sacct permet d’interroger la base de données de SLURM afin de suivre la consommation des ressources :\n",
    "\n",
    "Consulter les informations de bases d'un job : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sacct -j $job_id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Afficher des informations détaillées d'un job :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sacct --format=JobID,JobName,State,Start,Elapsed,CPUTime,ReqCPUS,NodeList -j $job_id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### SLURM job efficiency\n",
    "\n",
    "https://ifb-elixirfr.gitlab.io/cluster/doc/slurm/slurm_efficiency/ \n",
    "\n",
    "`sreportseff` prend un `jobid` et report sur l'efficacité de l'utilisation des cpu et de la memoire."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "module load reportseff\n",
    "\n",
    "reportseff $job_id"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bonnes pratiques pour la reproductibilité - la suite\n",
    "\n",
    "<img src=\"images/FAIR_data_principles.png\" alt=\"drawing\" width=\"100\" style=\"float: right;\"/>\n",
    "\n",
    "\n",
    "### Tracer l'usage des ressources consommées\n",
    "\n",
    "Il y a plusieurs intérêt à regarder en post-mortem l'usage en ressource de ses jobs:\n",
    " - Pour adapter à la baisse si possible pour sont prochain run \n",
    " - Tracer l'usage pour aider à l'utilisation de son script/workflow d'analyse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
