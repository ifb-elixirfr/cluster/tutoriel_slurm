# Tutoriels SLURM

Ce répertoire comprend 2 tutoriels: 
- tutoriel*.ipynb
- snakemake_tutirial.ipynb

## Démarrer

1. Démarrer un serveur JupyterLab en utilisant l'un des services [JupyterHub](https://jupyterhub.cluster.france-bioinformatique.fr) ou [Open on Demand](https://ondemand.cluster.france-bioinformatique.fr) proposés par l'IFB (par exemple en utilisant
1. Lancer un Terminal
1. Lancer la commande :  
```
git clone https://gitlab.com/ifb-elixirfr/cluster/tutoriel_slurm.git
```
1. Naviguez dans le répertoire `tutoriel_slurm` ainsi créé et ouvrez le notebook `tutoriel.ipynb`



## tutoriel*.ipynb

Ces tutoriels SLURM se présentent sous la forme d'un notebook Jupyter Bash.

### tutoriel.ipynb
En suivant ce tutoriel vous apprendez :
* le vocabulaire SLURM
* à soumettre des jobs
* à suivre l'execution de vos jobs
* à optimiser les performances d'execution de vos jobs
* à paralélliser des jobs

### tutoriel-light.ipynb
En suivant ce tutoriel vous apprendez :
* le vocabulaire SLURM
* à soumettre des jobs (en mode paraléllisé)
* à suivre l'execution de vos jobs

## snakemake_tutorial.ipynb
Ce tutoriel explique comment exécuter un flux de travail Snakemake sur une infrastructure HPC et comment gérer les dépendances du flux de travail.